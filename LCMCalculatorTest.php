<?php
require('LCMCalculator.class.php');

class LCMCalculatorTest extends PHPUnit_Framework_TestCase {
	/**
	 * Test that the LCM Calculator extends the Calculator
	 */
	public function testConstruct() {
		$lcm = new LCMCalculator();
		$this->assertInstanceOf('Calculator', $lcm);
	}

	/**
	 * Find the LCM of numbers
	 * 
	 * @dataProvider dataCalculate
	 */
	public function testCalculate($primeFactorsets, $expectedLCM) {
		$lcm = new LCMCalculator();
		$mocks = array();
		foreach($primeFactorsets as $primeFactors) {
			$mock = $this->getMockBuilder('CountingNumber')
				->disableOriginalConstructor()
				->setMethods(array('getPrimeFactors'))
				->getMock();

			$mock->expects($this->once())
				->method('getPrimeFactors')
				->will($this->returnValue($primeFactors));
			$mocks[] = $mock;
		}
		$lcm->add($mocks);
		$this->assertEquals($expectedLCM, $lcm->calculate());
	}

	public function dataCalculate() {
		return array(
			'Two Numbers' => array(
				'PrimeFactorSets' => array(
					array(2=>3, 5=>1),
					array(4=>1)),
				'Expected LCM' => 160),
			'Two numbers with shared factors' => array(
				'PrimeFactorSets' => array(
					array(2=>2, 3=>2),
					array(2=>1, 5=>1)),
				'Expected LCM' => 180),
			'Three numbers' => array(
				'PrimeFactorSets' => array(
					array(2=>3),
					array(5=>1, 3=>2),
					array(2=>1, 5=>2, 3=>1)),
				'Expected LCM' => 1800),
			);
	}
}
