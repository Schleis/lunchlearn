<?php

abstract class Calculator {	
	abstract public function calculate();

	public function add($data) {
		if(is_array($data)){
			foreach($data as $value){
				$this->data[] = $value;
			}
		} else {
			$this->data[] = $data;
		}
	}
	
	public function getData() {
		return $this->data;
	}
}
