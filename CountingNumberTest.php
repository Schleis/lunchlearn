<?php
require_once('CountingNumber.class.php');

class CountingNumberTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * Creating a CountingNumber object
	 * Only positive nonzero integers are valid values for counting numbers
	 *
	 * @dataProvider dataCountingNumber
	 */
	public function testConstruct($value, $expectedPrimes, $expectedException = null) {
		if(! is_null($expectedException)) {
			$this->setExpectedException($expectedException['type'], $expectedException['message']);
		}

		$countingNumber = new CountingNumber($value);
		$this->assertEquals(intval($value), $countingNumber->getValue());
		$this->assertAttributeEquals($expectedPrimes, 'primeList', $countingNumber);
	}

	public function dataCountingNumber() {
		return array(
			array(2, array(2)),
			array(6, array(2,3,5)),
			array('10', array(2,3,5,7,)),
			array(1, array()),
			array(0, array(), array('type' => 'InvalidArgumentException', 'message' => 'Only positive integers greater than zero are counting numbers')),
			array(-1, array(), array('type' => 'InvalidArgumentException', 'message' => 'Only positive integers greater than zero are counting numbers')),
			array('Four', array(), array('type' => 'InvalidArgumentException', 'message' => 'Only positive integers greater than zero are counting numbers')),
			array(new stdClass(), array(), array('type' => 'InvalidArgumentException', 'message' => 'Only positive integers greater than zero are counting numbers')),
			array(array(), array(), array('type' => 'InvalidArgumentException', 'message' => 'Only positive integers greater than zero are counting numbers')),
		);
	}

	/**
	 * Get the prime factors of the number
	 *
	 * @dataProvider dataPrimeFactorization
	 */
	public function testPrimeFactorization($value, $expectedFactorization) {
		$countingNumber = new CountingNumber($value);
		$this->assertEquals($expectedFactorization, $countingNumber->getPrimeFactors());
	}

	public function dataPrimeFactorization() {
		return array(
			array(8, array(2=>3)),
			array(20, array(2=>2, 5=>1)),
			array(11, array(11=>1)),
		);
	}
	
}
