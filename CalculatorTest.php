<?php
require_once('Calculator.class.php');

/**
 * Test the abstract Calculator Objects concrete methods
 */
class CalculatorTest extends PHPUnit_Framework_TestCase {
	/**
	 * Test adding a parameter to the calculator
	 * @dataProvider dataAddNumber
	 */
	public function testAddNumber($data, $expectedData) {
		$calculator = $this->getMockForAbstractClass('Calculator');
		$calculator->add($data);
		$this->assertEquals($expectedData, $calculator->getData());
		
		return $calculator;
	}
	
	public function dataAddNumber() {
		return array(
			'One Piece of data' => array(2, array(2)),
			'More than one piece of data' => array(array(1,2,3), array(1,2,3)),
		);
	}
	
	/**
	 * Test adding a second number
	 */
	public function testAddAnotherNumber(){
		$calculator = $this->getMockForAbstractClass('Calculator');	
		$calculator->add(3);
		$calculator->add(2);
		$this->assertEquals(array(3,2), $calculator->getData());
		
		return $calculator;
	}
	
	/**
	 * Test adding an array of numbers
	 * @depends testAddAnotherNumber
	 */
	public function testAddArray($calculator) {
		$expectedData = $calculator->getData();
		$expectedData = array_merge($expectedData, array(4,5));
		$calculator->add(array(4,5));
		$this->assertEquals($expectedData, $calculator->getData());
	}
}
