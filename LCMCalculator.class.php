<?php
require_once('Calculator.class.php');

class LCMCalculator extends Calculator {

	protected $LCMFactors = array();
	/**
	 * Calculate the Lowest Common Denominator
	 * 
	 * Find the prime factorization of each number.
	 * Take the highest power of each.  The product of the powers is the LCM
	 */
	public function calculate() {
		$this->findLCMFactors();	
		$product = 1;
		foreach($this->LCMFactors as $factor => $power){
			$product *= pow($factor, $power);
		}
		return $product;
	}

	protected function findLCMFactors() {
		foreach($this->data as $number) {
			$factors = $number->getPrimeFactors();
			foreach($factors as $factor => $power) {
				if(in_array($factor, array_keys($this->LCMFactors))){
					if($power > $this->LCMFactors[$factor]){
						$this->LCMFactors[$factor] = $power;
					}
				} else {
					$this->LCMFactors[$factor] = $power;
				}
			}
		}
	}
}
