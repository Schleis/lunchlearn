<?php

class CountingNumber {
	protected $value;
	protected $primeList = array();

	public function __construct($value) {
		$this->setValue($value);
		$this->setPrimeList();
	}

	/**
	 * Get the prime factorization of the value
	 */
	public function getPrimeFactors() {
		$tempValue = $this->value;
		
		$factors = array();
		foreach ($this->primeList as $prime) {
			while (0 == $tempValue % $prime && 1 != $tempValue) {
				//remove one power of the prime and get the new value
				$tempValue = $tempValue / $prime;

				//check if we have this factor already
				if (! array_key_exists($prime, $factors)) {
					$factors[$prime] = 1;
				} else {
					$factors[$prime]++;
				}
			}
			
			if(1 == $tempValue) {
				break;
			}
		}
		return $factors;
	}

	/**
	 * Create a list of primes lower than the value of the number
	 * Uses the Sieve of Erastothene
	 */
	protected function setPrimeList() {
		//get list of numberList less than number starting at 2
		$numberList = array();
		for ($i = 2; $i <= $this->value; $i++){
			$numberList[] = $i;
		}
		
		$primes = array();
		while (0 != count($numberList)) {
			//first value is a prime
			$prime = array_shift($numberList);
			$primes[] = $prime;

			//remove all multiples of the number
			foreach ($numberList as $key => $number) {
				if (0 == $number % $prime) {
					unset($numberList[$key]);
				}
			}
			array_values($numberList);
		}
		$this->primeList = $primes;

	}

	protected function setValue($value) {
		if(is_object($value) || 1 > (int)$value){
			throw new InvalidArgumentException('Only positive integers greater than zero are counting numbers');
		}

		$this->value = intval($value);
	}

	public function getValue() {
		return $this->value;
	}
}
